export APP_IMAGE_NAME = golang-cicd-app
export APP_IMAGE_TAG = 1.0
export APP_PORT = 3000

build:
	docker build -t ${APP_IMAGE_NAME}:${APP_IMAGE_TAG} .

run:
	docker-compose up

compile:
	echo "Compiling for every OS and Platform"
	GOOS=linux GOARCH=arm go build -o bin/main-linux-arm main.go
	GOOS=linux GOARCH=arm64 go build -o bin/main-linux-arm64 main.go
	GOOS=freebsd GOARCH=386 go build -o bin/main-freebsd-386 main.go