FROM  golang:1.17.1-alpine AS builder

# All these steps will be cached
RUN mkdir /app
WORKDIR /app
# Copy go.mod and go.sum files to workspace
COPY ./app/go.mod ./

# Get dependencies, will also be cached if you don't change mod/sum
RUN go mod download
# Copy the source code
COPY . .
# Build the binary
RUN CGO_ENABLED=0  GOOS=linux GOARCH=arm64 go build -o /go/bin/main.go ./app/main.go
FROM scratch
COPY --from=builder /go/bin/main.go /main.go
# CMD
CMD ["/main.go"]
