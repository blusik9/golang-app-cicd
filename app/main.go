package main

import (
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/helloworld", helloHandler)
	log.Fatal(http.ListenAndServe(":3000", nil))
}

func helloHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("Received request: %s %s", r.Method, r.URL.Path)
	w.Write([]byte("Hello, World!"))
}
